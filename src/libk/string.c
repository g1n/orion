#include <string.h>

size_t strlen(const char *s) {
  int i = 0;
  while(s[i] != '\0') {
    i++;
  }
  return i;
}

void *memmove(void *dest, const void *src, size_t n) {
  unsigned char* chardest = dest;
  const unsigned char* charsrc = src;

  if (chardest == charsrc) {
    return (void*)chardest;
  } else if (chardest > charsrc) {
    for (size_t i = n; i > sizeof(charsrc); i--)
      chardest[i] = charsrc[i];
  } else {
    for (int i = 0; i < chardest - charsrc; i++)
      chardest[i] = charsrc[i];
  }

  memcpy(chardest, charsrc, n);

  return (void*)chardest;
}

int memcmp(const void *s1, const void *s2, size_t n) {
  int status = sizeof(s1) - sizeof(s2);
  if (status > 0 || status < 0) {
    return status;
  } else {
    const unsigned char* first = s1;
    const unsigned char* second = s2;
    for (size_t i = 0; i < n; i++) {
      if (first[i] != second[i])
        return first[i] - second[i];
    }
    return 0;
  }
}

void *memset(void *s, int c, size_t n) {
  unsigned char uc = c;
  unsigned char* ss = s;
  for (size_t i = 0; i < n; i++)
    ss[i] = uc;
  return (void*)ss;
}

void *memcpy(void *restrict dest, const void *restrict src, size_t n) {
  size_t i;
  unsigned char* chardest = dest;
  const unsigned char* charsrc = src;
  for (i = 0; i < n && charsrc[i] != '\0'; i++) {
    chardest[i] = charsrc[i];
  }
  for (; i < n; i++) {
    chardest[i] = '\0';
  }
  return (void*)chardest;
}
