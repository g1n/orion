#include <paging.h>
#include <stdio.h>
#include <string.h>

uint32_t page_dir[1024] __attribute__((aligned(4096)));
uint32_t page_table[1024] __attribute__((aligned(4096)));

void page_fault(registers_t regs) {
  uint32_t faulting_address;
  __asm__ volatile("mov %%cr2, %0" : "=r" (faulting_address));
   
  // The error code gives us details of what happened.
  int present   = !(regs.err_code & 0x1); // Page not present
  int rw = regs.err_code & 0x2;           // Write operation?
  int us = regs.err_code & 0x4;           // Processor was in user-mode?
  int reserved = regs.err_code & 0x8;     // Overwritten CPU-reserved bits of page entry?
  //int id = regs.err_code & 0x10;          // Caused by an instruction fetch?
  
  kprintf("Page fault: ( ");
  if (present) {kprintf("present ");}
  if (rw) {kprintf("read-only ");}
  if (us) {kprintf("user-mode ");}
  if (reserved) {kprintf("reserved ");}
  kprintf(") at 0x%x\n", faulting_address);
  kprintf("PAGE FAULT\n");
}

void load_directory(uint32_t *dir) {
  __asm__ volatile("mov %0, %%cr3":: "r"(dir));
}

void enable_paging() {
  uint32_t cr0;
  __asm__ volatile("mov %%cr0, %0": "=r"(cr0));
  cr0 |= 0x80000001; // Enable paging!
  __asm__ volatile("mov %0, %%cr0":: "r"(cr0));
}

extern uint32_t endkernel;

page_t *next_page;
page_t *free_pages = 0;

page_t *kalloc_frame(){
  page_t *temp;
  if (free_pages == 0) {
    temp = next_page;
    next_page += PAGE_SIZE;
  } else {
    temp = free_pages;
    free_pages = free_pages->next;
  }
  return temp;
}

void kfree_frame(page_t *page) {
  page->next = free_pages;
  free_pages = page;
}

void paging_init() {
  unsigned int i;
  for(i = 0; i < 1024; i++) {
    page_dir[i] = 0x00000002;
  }

  next_page = (page_t*)endkernel;

  for(i = 0; i < 1024; i++) {
    page_table[i] = (i * 0x1000) | 3;
  }

  page_dir[0] = ((unsigned int)page_table) | 3;
  
  register_interrupt_handler(14, &page_fault);
  load_directory(page_dir);
  enable_paging();
}
