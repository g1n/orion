#include <idt.h>
#include <vga.h>
#include <paging.h>
#include <stdio.h>
#include <stdlib.h>
#include <serial.h>

void early_kernel_main() {
  serial_init();
  terminal_init();
  idt_init();
  paging_init();
}
